# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 16:50:48 2018

@author: PSJ

to speed up load, tile, save image
try this candidates.
scikit (skimage) -- surrently using
keras
OepnCV
Qyqtgraph
"""

#import sys
import os
import socket
import math
import time
import numpy as np
from PIL import Image
import skimage.io as io
import matplotlib as mpl
import matplotlib.path as mpath
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
import matplotlib.patheffects as pe
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import gpxpy.parser as parser
from pyproj import Transformer, transform  #Should install pyproj module.
import fitdecode
mpl.use('Qt5Agg')
#mpl.use('Qt5Cairo')
#mpl.use('Cairo')

#bound_str = f'{name} : {lati1}, {lati2}, {longi1}, {longi2}'

'''
Caution: tile 0, 0 is not pix coord (0,0)

level 8 (1810uis, 1906plw), level 2 (1909dms)
    x bound [0,3], y bound [0,4], offset [0,0]

    1024 x 1280 px

level 9 (1810uis, 1906plw), level 3 (1909dms)
    x bound [0,7], y bound [0,9], offset [0,0]
    2048 x 2560 px

level 10 (1810uis, 1906plw), level 4 (1909dms)
    x bound [0,14], y bound [0,19], offset [0,0]
    3840 x 5120 px

level 11 (1810uis, 1906plw), level 5 (1909dms)
    x bound [1,28], y bound [1,38], offset [256,256]
    7168 x 9728 px

level 12 (1810uis, 1906plw), level 6 (1909dms)
    x bound [2,56], y bound [2,77], offset [512,512]
    14080 x 19456 px

level 13 (1810uis, 1906plw), level 7 (1909dms)
    x bound [4,113], y bound [4,153], offset [1024,1024]
    28160 x 38400 px

level 14 (1810uis, 1906plw), level 8 (1909dms)
    x bound [9,226], y bound [10,307], offset [2304,2560]
    55808 x 76288
'''


def get_member(o,kind=None):
    if kind is None or kind == 'all':
        return [a for a in dir(o)]
    elif kind == 'sys':
        return [a for a in dir(o) if a.startswith('__')]
    elif kind == 'user':
        return [a for a in dir(o) if not a.startswith('__')]
    elif kind == 'func' or kind == 'callables' or kind == 'functions':
        return [a for a in dir(o) if not a.startswith('__') and callable(getattr(o,a))]
    elif kind == 'var' or kind =='vars' or kind == 'variables' or kind =='variable':
        return [a for a in dir(o) if not a.startswith('__') and not callable(getattr(o,a))]


def pr_member(o,kind=None):
    a = get_member(o,kind)
    print(a)


class BBox:
    __slots__ = ('x0', 'x1', 'y0', 'y1')

    def __init__(self, x0=None, x1=None, y0=None, y1=None):
        self.x0 = x0
        self.x1 = x1
        self.y0 = y0
        self.y1 = y1

    '''
    def __init__(self, xl, yl):
        self.x0 = xl[0]
        self.x1 = xl[1]
        self.y0 = yl[0]
        self.y1 = yl[1]
    '''

    def __iter__(self):
        return (self.y0, self.y1, self.x0, self.x1,).__iter__()

    def __str__(self):
        return '(%s,%s), (%s,%s)' % (self.x0, self.y0, self.x1, self.y1)

    def to_list(self):
        return [self.x0, self.x1], [self.y0, self.y1]

    def xspan(self):
        return self.x1 - self.x0

    def yspan(self):
        return self.y1 - self.y0


def set_alpha(img,alpha):
    data     = img.getdata()
    newData  = []
    # loop through pixels in planeImg
    for item in data:
        # item[3] is the current pixel alpha value. When 0: transparent area on png, leave it at 0
        # item[{0, 1, 2}] is pixel {R,G,B} value
        if item[3] <= alpha:
            newData.append( (item[0], item[1], item[2], item[3]) )
        # else the current pixel is a part of the plane: set alpha to desired value
        else:
            newData.append( (item[0], item[1], item[2], alpha) )
    img.putdata( newData )
    return img


def pixel_len(x0,y0,x1,y1):
    return math.sqrt((x0-x1)*(x0-x1)+(y0-y1)*(y0-y1))


def get_fit(f_name):
    '''
    Read Fit file
    Some fit file has no gps data. eg: indoor training log
    '''
    track = dict()
    lat_list = []
    long_list = []
    # FitEofError, FitCRCError
    with fitdecode.FitReader(f_name) as fit:
        try:
            i = 0
            for frame in fit:
                if isinstance(frame, fitdecode.FitDataMessage):
                    i += 1
                    if frame.name == 'record':
                        try:
                            pass
                        except IndexError:
                            print(i, '       Index error')
    
                        lat = 0
                        long = 0
                        for n in frame.fields:
                            #print('    ', n.name, n.value, n.units)
                            if n.name == 'position_lat':
                                lat = (n.value*180)/pow(2,31)
                            if n.name == 'position_long':
                                long = (n.value*180)/pow(2,31)
    
                        if lat != 0 and long != 0:
                            lat_list.append(lat)
                            long_list.append(long)
        except (FitEofError, FitCRCError) as err:
        #except FitEofError as err:
            print(f'Unexpected EOF Error while reading file: {f_name}')

        #except FitCRCError as err:
            print(f'CRC Error while reading file: {f_name}')

    track = dict(lati=lat_list, long=long_list, ways=[])
    #print(track)
    print(f'Length: {len(track)}, {len(track["lati"])}')
    return track


def get_gpx(f_name):
    '''
    Read Gpx file
    '''
    gpx_file = open( f_name, 'r', encoding='UTF8')

    gpx_parser = parser.GPXParser( gpx_file )
    #gpx_parser.parse()

    gpx_file.close()

    gpx = gpx_parser.parse()

    # no trkpt to process
    try:
        trklen = len(gpx.tracks[0].segments[0].points)
    except IndexError as err:
        trklen = 0
    finally:
        if trklen == 0:
            print("Error: No track points to process.\n")
            return None
    return gpx


def deep_view(gpx):
    '''
    Extract lati, long, ways from gpx class
    '''
    lati = []
    long = []
    waypoints = []

    pts = gpx.tracks[0].segments[0].points
    for i,p in enumerate(pts):
        lati.append(p.latitude)
        long.append(p.longitude)

    x = []
    y = []
    n = []
    for i,w in enumerate(gpx.waypoints):
        x.append(w.longitude)
        y.append(w.latitude)
        n.append(w.name)
    waypoints.extend([x,y,n])
    print(f'Track points = {len(lati)}, Waypoints = {len(waypoints)}')
    return dict(lati=lati, long=long, ways=waypoints)


def wgs84_to_daum(xlist,ylist):
    '''
    Convert coordinate system WGS84 to DAUM
    '''
    transformer = Transformer.from_proj(4326, 5181)     #WGS84 2 wcongnamul(DAUM)
    #print(len(xlist), len(ylist))

    longitude= []
    latitude = []
    for x, y in zip(xlist, ylist):
        nx, ny = transformer.transform(y,x)
        longitude.append(nx)
        latitude.append(ny)
        #print(len(latitude), x,y, nx,ny)

    return latitude, longitude


def daum2pic(xlist, ylist, level=8):
    '''
Convert coordinate system DAUM to pixel, non cropping
level 8: [59,1280-118] [512]
level 9: [117,2560-235] [256]
level 10: [234,5120-469] [128]
level 11: [212,10240-937] [64]
level 12: [213,20480-1875] [32]
level 13: [213,40960-3750] [16]
level 14: [213,81920-7500] [8]
    '''
    #print(level)
    a_coeff = [0,0,0,0,0, 0,0,0,512,256,
               128,64,32,16,8, 4,2,1,.5,.25]
    x_coeff = [0,0,0,0,0, 0,0,0,59,117,
               234,468,938,1876,3751, 0,0,0,0,0]
    y_coeff = [0,0,0,0,0, 0,0,0,1280-118,2560-235,
               5120-469,10240-937,20480-1875,40960-3750,81920-7500, 0,0,0,0,0]

    if level < 8 or level > 14:
        level = 8
    a   = a_coeff[level]
    x_b = x_coeff[level]
    y_b = y_coeff[level]

    long = []
    lati = []
    #print(level, a,x_b,y_b)
    for x, y in zip(xlist, ylist):

        #print(x,a,x_b)
        x = int(x/a + x_b)
        y = int(-y/a + y_b)
        long.append(x)
        lati.append(y)

    #print('pic coord: ',x,y)
    return long,lati


def pic2tile_new(x, y, bbox, level=8):
    '''
    x, y : array coordinate to convert
    bbox : bounding box
    level : map level
    '''
    #x_coeff = [0,0,0,0,0, 0,0,0,1024,2048,
    #           3840,7680,14080,28160,56320, 0,0,0,0,0]
    y_coeff = [0,0,0,0,0, 0,0,0,1280,2560,
               5120,10240,20480,40960,81920, 0,0,0,0,0]

    if level < 8 or level > 14:
        level = 8
    #x2 = x_coeff[level]
    y2 = y_coeff[level]

    zerox = bbox.x0*256
    zeroy = y2-bbox.y0*256
    spanx = 1
    spany = -1

    #print(f'span = {spanx}, {spany},  zero = {zerox}, {zeroy}')
    tilex = []
    tiley = []
    for i in range(len(x)):
        xx = spanx * (x[i]-zerox)
        yy = spany * (y[i]-zeroy)
        tilex.append(xx)
        tiley.append(yy)
        #print(xx,yy)
    return tilex, tiley

def inverse_y( ylist, bbox):
    span = (bbox.y1-bbox.y0)*256
    a = []
    for y in ylist:
        a.append(span - y)
    #print(ylist)
    #print(a)
    return a

def get_width(level):
    '''
    get course line width
    '''
    w_arr = [4,4,4,4, 4,4,4,4,
             4,5,4,6, 6,7,7,8, 8]
    if level >=0 and level <=16:
        return w_arr[level]


def get_alpha(level):
    '''
    get course line width
    '''
    a_arr = [.8,.8,.8,.8, .8,.8,.8,.8,
             .8,.75,.75,.75, .75,.75,.7, .7, .7]
    if level >=0 and level <=16:
        return a_arr[level]


def overlay_fit(ax, bbox, trk, level, color='b', alpha=.8, width=0):
    pass


def overlay_gpx(ax ,bbox, trk, level, color='b', alpha=.7, width=0, method=0):
    '''
    Read gpx file, Convert coordinate and Plot a track to ax
    level is map level
    default DAUM map
    '''
    '''
    #x,y = wgs84_to_daum(x,y)        #caution! x=lat, y=long
    x,y = daum2pic(trk['long'], trk['lati'], level)       #caution! x=lat, y=long
    print('before=',x[0],y[0])
    x,y = pic2tile_new(x, y, bbox, level)
    print('after=',x[0],y[0])
    y = inverse_y(y, bbox)
    '''

    if width == 0:
        lwidth = get_width(level)
    elif width <= 10 and width >= 2:
        lwidth = width
    else:
        lwidth = 4
    if alpha > 1:
        alpha  = get_alpha(level)

    ax.set_autoscale_on(False)
    x = trk['long']
    y = trk['lati']
    if method == 0:
        ax.plot(x,y, lw=lwidth, color=color,alpha=alpha,path_effects=[pe.Stroke(alpha=alpha,linewidth=lwidth+3,foreground='w'),pe.Normal()],zorder=3)
    else:
        ax.plot(x,y, lw=lwidth+3, color='w',alpha=alpha,zorder=3)
        ax.plot(x,y, lw=lwidth, color=color,alpha=alpha,zorder=3)
        #p.set_path_effects([pe.Stroke(linewidth=lwidth+4,color='w'),pe.Normal()])

    return x,y


def wgs84_2_pix(bbox, way, level, y_adj=0):
    '''
    Convert waypoint to picture coordinate
    level is map level
    default DAUM map
    '''
    x = []
    y = []
    name = []
    #for i, point in enumerate(way):
    if len(way) > 0:
        for i in range(len(way)):
            x.append(way[i].longitude)
            y.append(way[i].latitude)
            name.append(way[i].name)

        x,y = wgs84_to_daum(x,y)        #caution! x=lat, y=long
        x,y = daum2pic(x,y,level)       #caution! x=lat, y=long
        x,y = pic2tile_new(x,y,bbox,level)
        if y_adj != 0:
            for i in range(len(y)):
                y[i] += y_adj
    return x,y,name


def calc_wpt(bbox, way, level, y_adj=9):
    '''
    Convert waypoint to picture coordinate
    level is map level
    default DAUM map
    '''
    #for i, point in enumerate(way):
    if len(way) > 0:
        x,y = daum2pic(way[0], way[1], level)       #caution! x=lat, y=long
        x,y = pic2tile_new(x, y, bbox, level)
        if y_adj != 0:
            for i in range(len(y)):
                y[i] += y_adj
    return x, y, way[2]


def get_bound(a_lst):
    minx_daum = -11560.087363551313       # 124.73344
    maxx_daum = 434871.51448555756       # 129.69622
    miny_daum = -39504.05836736236       # 33.116847
    maxy_daum = 570767.9451421077       # 38.606459
    #([-11560.087363551313, 434871.51448555756], [-39504.05836736236, 570767.9451421077])
    #print(a_lst['long'])
    x0 = min(a_lst['long'])
    x1 = max(a_lst['long'])
    y0 = min(a_lst['lati'])
    y1 = max(a_lst['lati'])
    #print(x0,x1,y0,y1)
    if (x0 < minx_daum or x0 > maxx_daum or x1 < minx_daum or x1 > maxx_daum or 
        y0 < miny_daum or y0 > maxy_daum or y1 < miny_daum or y1 > maxy_daum):
        return BBox(x0=1000000, x1=-1000000, y0=1000000, y1=-1000000)       # false boundary
    else:
        return BBox(x0=x0,x1=x1,y0=y0,y1=y1)


def get_bound_daum(a_list):
    b = BBox(x0=1000000,x1=-10000001,y0=1000000,y1=-1000000)
    for i, d in enumerate(a_list):
        #print(wgs84_to_daum([124.73344,129.69622],[33.116847,38.606459]))
        if len(d['lati']) > 0:
            b1 = get_bound(d)
            a_list[i]['bbox'] = b1
            print(f'Bbox of {i} = {a_list[i]["bbox"]}')
            if b.x0 > b1.x0:
                b.x0 = b1.x0
            if b.x1 < b1.x1:
                b.x1 = b1.x1

            if b.y0 > b1.y0:
                b.y0 = b1.y0
            if b.y1 < b1.y1:
                b.y1 = b1.y1
    return b

def get_tile_bound(bbox, level=8, border=25):
    '''
    get image boundary
    Coordinate system: Tile number
    '''
    x, y = bbox.to_list()

    #p_name = 'Initial(wgs84)'
    #print(f'  {p_name}:  {x[0]:.6f}, {x[1]:.6f},  {y[0]:.6f}, {y[1]:.6f}')
    #x,y = wgs84_to_daum(x,y)

    p_name = 'wgs84 to DAUM '
    print(f'  {p_name}:  {x[0]:.2f}, {x[1]:.2f},  {y[0]:.2f}, {y[1]:.2f}')

    x,y = daum2pic(x,y,level)
    p_name = 'DAUM to pic   '
    print(f'  {p_name}:  {x[0]}, {x[1]},  {y[0]}, {y[1]}')

    x,y = pic2tile_new(x,y,BBox(0,0,0,0),level)
    p_name = 'Pic to Tile 0 '
    print(f'  {p_name}:  {x[0]}, {x[1]},  {y[0]}, {y[1]}')

    x[0] = int((x[0]-border)/256)
    x[1] = int((x[1]+border)/256)+1
    y[0] = int((y[0]-border)/256)
    y[1] = int((y[1]+border)/256)+1
    p_name = 'Pic to Tile 1 '
    print(f'  {p_name}:  {x[0]}, {x[1]},  {y[0]}, {y[1]}')

    return BBox(x[0],x[1],y[0],y[1])


def get_impath_1909(mkind='2d/', level=8):
    if level >= 8 and level <= 14:
        base_path_pc = 'F:/Src2/Map/daum_'
        base_path_laptop = 'e:/SRC/Map/daum_'
        map_version = '1909dms_'

        if socket.gethostname() == 'PSJ-PC6':
            tile_basepath = base_path_pc+map_version+mkind+str(level-6)+'/'
        else:
            tile_basepath = base_path_laptop+map_version+mkind+str(level-6)+'/'

        return tile_basepath
    else:
        return None


def load_tile_1909_2d(fig, bbox, level=8):
    '''
    Load daum map 2019, 09 version
    fig : ws figure canvas
    xl, yl : tile bound
    level : map level
    '''
    tile_basepath = get_impath_1909('2d/', level)
    print(tile_basepath)
    spanx = bbox.xspan()
    spany = bbox.yspan()

    t1 = 0
    t2 = 0
    imyl = []
    imy = np.empty((spany*256, spanx*256, 4), dtype=np.int8)
    for y in range(bbox.y1-1,bbox.y0-1,-1):
        imxl = []

        for x in range(bbox.x0,bbox.x1):
            im_path = tile_basepath+str(y)+'/'+str(y)+'_'+str(x)+'.png'
            dt = time.time()
            imxl.append(io.imread(im_path))
            dt = time.time() - dt
            t1 += dt
            dt = time.time()

        imyl.append(np.hstack(imxl))
    try:
        imy = np.vstack(imyl)
    except ValueError:
        for im in imyl:
            print(f'Error in: {x}:{y}, {im.ndim} {im.size}')
    dt = time.time()
    ax = plt.Axes(fig, [0, 0, 1, 1])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(imy, aspect='equal')
    dt = time.time() - dt
    t2 = dt
    

    print(f'Loading time: {t1:.3f}, {t2:.3f}')
    '''
    print(imy.shape)
    dt = time.time()
    io.imsave('test001.jpg', imy[:,:,:3], quality=90)
    dt = time.time() - dt
    print(f'Test saving time: {dt:.3f}')
    '''
    return ax


def load_tile_1909_sky(fig, bbox, level=8, gamma=1):
    '''
    Load daum map sky picture 2019, 09 version
    '''
    tile_basepath = get_impath_1909('s/', level)
    spanx = bbox.xspan()
    spany = bbox.yspan()

    t1 = 0
    t2 = 0
    imyl = []
    imy = np.empty((spany*256, spanx*256, 4), dtype=np.int8)
    for y in range(bbox.y1-1,bbox.y0-1,-1):
        imxl = []

        for x in range(bbox.x0,bbox.x1):
            im_path = tile_basepath+str(y)+'/'+str(y)+'_'+str(x)+'.jpg'
            dt = time.time()
            im = io.imread(im_path)
            imxl.append(im)

            dt = time.time() - dt
            t1 += dt
            dt = time.time()

        imyl.append(np.hstack(imxl))
    try:
        imy = np.vstack(imyl)
    except ValueError:
        for im in imyl:
            print(f'Error in: {x}:{y}, {im.ndim} {im.size}')
    dt = time.time()
    ax = plt.Axes(fig, [0, 0, 1, 1])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(imy, aspect='equal')
    dt = time.time() - dt
    t2 = dt

    print(f'Loading time: {t1:.3f}, {t2:.3f}')
    return ax


def load_tile_1909_hybrid(fig, bbox, level=8, gamma=1):
    '''
    Load daum map 2d+sky image 2019, 09 version
    '''
    tile_basepath = get_impath_1909('s/', level)
    spanx = bbox.xspan()
    spany = bbox.yspan()

    t1 = 0
    t2 = 0
    imyl = []
    imy = np.empty((spany*256, spanx*256, 4), dtype=np.int8)
    for y in range(bbox.y1-1,bbox.y0-1,-1):
        imxl = []

        for x in range(bbox.x0,bbox.x1):
            im_path = tile_basepath+str(y)+'/'+str(y)+'_'+str(x)+'.jpg'
            dt = time.time()
            im = io.imread(im_path)
            imxl.append(im)

            dt = time.time() - dt
            t1 += dt
            dt = time.time()

        imyl.append(np.hstack(imxl))
    try:
        imy = np.vstack(imyl)
        if gamma != 1 and gamma >= 0.5 and gamma <= 2.0:
            imy = np.array(255*(imy / 255.) ** gamma, dtype='uint8')
    except ValueError:
        for im in imyl:
            print(f'Error in: {x}:{y}, {im.ndim} {im.size}')
    dt = time.time()
    ax = plt.Axes(fig, [0, 0, 1, 1])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(imy, aspect='equal')
    dt = time.time() - dt
    t2 = dt

    print(f'Loading time sky : {t1:.3f}, {t2:.3f}')

    tile_basepath = get_impath_1909('h/', level)
    t3 = 0
    t4 = 0
    imyl = []
    imy = np.empty((spany*256, spanx*256, 4), dtype=np.int8)
    for y in range(bbox.y1-1,bbox.y0-1,-1):
        imxl = []

        for x in range(bbox.x0,bbox.x1):
            im_path = tile_basepath+str(y)+'/'+str(y)+'_'+str(x)+'.png'
            dt = time.time()
            im = io.imread(im_path)
            imxl.append(im)

            dt = time.time() - dt
            t3 += dt
            dt = time.time()

        imyl.append(np.hstack(imxl))
    try:
        imy = np.vstack(imyl)
        if gamma != 1 and gamma >= 0.5 and gamma <= 2.0:
            imy = np.array(255*(imy / 255.) ** gamma, dtype='uint8')
    except ValueError:
        for im in imyl:
            print(f'Error in: {x}:{y}, {im.ndim} {im.size}')
    dt = time.time()
    #ax = plt.Axes(fig, [0, 0, 1, 1])
    #ax.set_axis_off()
    #fig.add_axes(ax)
    ax.imshow(imy, aspect='equal')
    dt = time.time() - dt
    t4 = dt

    print(f'Loading time over: {t3:.3f}, {t4:.3f}')
    return ax


def draw_icon(ax,image_path, xl, yl):
    '''
    draws single icon or list of icons
    '''
    alpha = .85
    im0 = set_alpha(Image.open(image_path), int(alpha*256))
    #im0 = set_alpha(io.imread(image_path), int(alpha*256))
    im = OffsetImage(im0, zoom=0.5)
    if isinstance(xl, list):
        for x, y in zip(xl, yl):
            ax.add_artist(AnnotationBbox(im, (x, y), frameon=False))
    else:
        ax.add_artist(AnnotationBbox(im, (xl, yl), frameon=False))


def draw_waypoints(ax,fn,wxl,wyl,name,level):
    f_size = 16+level-9
    print(f'Font size= {f_size}')
    path9 = 'C:\\Windows\\Fonts\\DaehanB.ttf'
    fontprop2 = fm.FontProperties(fname=path9, size=f_size)
    alignment2 = {'horizontalalignment': 'center', 'verticalalignment': 'bottom'}
    #bboxval = dict(facecolor='#99ffff', alpha=0.5,edgecolor='#ddffff',boxstyle='round')
    bboxval = dict(facecolor='#55ff99', alpha=0.7,edgecolor='#ddff99',boxstyle='round')
    if len(wxl) > 0:
        draw_icon(ax, fn, wxl, wyl)

        for i,nm in enumerate(name):
            if len(nm) > 0:
                plt.text(wxl[i], wyl[i]-f_size-1, nm, fontproperties=fontprop2, **alignment2, color='black', bbox=bboxval)

def draw_markers(ax, dirs, color='b', level=8):
    '''
    angle unit : degree
    '''
    f = 'icon/Arrow_Right_001_50rw.png'

    alpha = .85
    im0 = set_alpha(Image.open(f), int(alpha*256))
    im = OffsetImage(im0, zoom=0.5)

    '''
    for i in range(len(dirs[0])):
        ax.add_artist(AnnotationBbox(im, (dirs[0][i], dirs[1][i]), frameon=False))
        pass
    '''

    #bbox_props = dict(boxstyle="rarrow", fc=(0.8, 0.9, 0.9), ec="b", lw=3)
    #bbox_props = dict(boxstyle="rarrow", fc=(1., 1., 1.), ec="b", lw=3)
    bbp1 = dict(boxstyle="rarrow", fc=color, ec='#ffffff', lw=6, alpha=.4)
    bbp2 = dict(boxstyle="rarrow", fc='#ffffff', ec=color, lw=3, alpha=.8)
    text = "        "
    for i in range(len(dirs[0])):
        t = ax.text(dirs[0][i], dirs[1][i], text, ha="center", va="center", rotation=dirs[2][i], size=5, bbox=bbp1)
        t = ax.text(dirs[0][i], dirs[1][i], text, ha="center", va="center", rotation=dirs[2][i], size=4, bbox=bbp2)
        bb = t.get_bbox_patch()
        bb.set_boxstyle("rarrow", pad=0.6)
    
    '''
    for i in range(len(dirs[0])):
        t = mpl.markers.MarkerStyle(marker=r'$\Rightarrow$')
        t._transform = t.get_transform().rotate_deg(dirs[2][i])
        ax.scatter(dirs[0][i], dirs[1][i], s=800, marker=t, color='b', zorder=5, alpha=.6)
        #ax.scatter(dirs[0][i], dirs[1][i], s=400, marker=(8, 0, dirs[2][i]+30), color='r', zorder=5)
    '''    
        

def getq(level):
    q_arr = [95,95,95,95,95,  95,95,95,95,95,
             95,95,90,85,80,  80,80,80,80,80]
    if level < 8 or level > 14:
        level = 8
    return q_arr[level]


def build_direction( trk, dist):
    x = []
    y = []
    d = []
    print(dist,len(trk['long']))
    for i in range(dist,len(trk['long'])-20,dist):
        x0 = trk['long'][i-1]
        x1 = trk['long'][i+1]
        y0 = trk['lati'][i-1]
        y1 = trk['lati'][i+1]
        #angle = (t0)
        #print (x0,x1,y0,y1)
        if x0 == x1:
            x0 += .0000001
        dd = math.atan2(y0-y1, x1-x0) * 360 / 2 / math.pi
        x.append(trk['long'][i])
        y.append(trk['lati'][i])
        d.append(dd)
        
    return [x,y,d]
        

def do_job(flist, mkind=2, level=8, width=0, sf=True, isway=True, gamma=1.0, mark=60):
    '''
    mkind : Map kind
    level : Map level
    width : Track line width
    sf : Draw Start / Finish marker if True
    isway : Draw Waypoints if True
    gamma : map image gamma correction value
    '''

    # bottom left coordinate  of map tile 0,0
    lati1 = 32.93
    long1 = 124.54

    daum = []
    t1 = time.time()
    print('==================================================')
    print(f'Kind: {mkind}, Level: {level}, Line width: {width}, S/F: {sf}, Waypoint: {isway}, Gamma: {gamma}')
    for fname, color in flist:
        print(f'File: {fname}')
        
        if fname[-3:].lower() == 'gpx':
            g = get_gpx( fname )
            a = deep_view(g)
        elif fname[-3:].lower() == 'fit':
            a = get_fit( fname )
        else:
            print(f'File type error !!! : {fname}')
            raise FileTypeError
            
        x, y  = wgs84_to_daum(a['long'],a['lati'])
        a['long'] = x
        a['lati'] = y
        if len(a['ways']) > 0:
            wx,wy = wgs84_to_daum(a['ways'][0], a['ways'][1])
            a['ways'][0] = wx
            a['ways'][1] = wy
        daum.append(a)

    bb1  = get_bound_daum(daum)
    if bb1.x0 == 1000000:
        return 'Nothing to plot.'
    bbox = get_tile_bound(bb1, level, border=30)
    print(f'bbox: {bbox}')

    fig = plt.figure(frameon=False,dpi=50)
    fig.set_size_inches(bbox.xspan()*256/50, bbox.yspan()*256/50)

    for i,d in enumerate(daum):
        try:
            #x,y = wgs84_to_daum(x,y)        #caution! x=lat, y=long
            x,y = daum2pic(d['long'], d['lati'], level)       #caution! x=lat, y=long
            print('before=',x[0],y[0])
            x,y = pic2tile_new(x, y, bbox, level)
            print('after=',x[0],y[0])
            y = inverse_y(y, bbox)
            daum[i]['long'] = x
            daum[i]['lati'] = y
        except IndexError:
            pass

    t_l = time.time()
    if mkind==2:
        ax = load_tile_1909_hybrid(fig, bbox, level, gamma)
        fn_tail = '_h'
    elif mkind==1:
        ax = load_tile_1909_sky(fig, bbox, level, gamma)
        fn_tail = '_s'
    else:
        ax = load_tile_1909_2d(fig, bbox, level)
        fn_tail = '_2d'
    t_l = time.time() - t_l

    
    #ax = plt.Axes(fig, [0,0,1,1])
    ax.set_axis_off()
    ax.set_autoscale_on(False)
    #ax.set_xbound(lower=0, upper=bbox.xspan()*256)
    #ax.set_ybound(lower=0, upper=bbox.yspan()*256)
    #fig.add_axes(ax)
    


    fn_start = 'icon/start_004_50b.png'
    fn_end = 'icon/end_001_50r.png'
    #fn_waypoint = 'icon/control_002_50b.png'
    fn_waypoint = 'icon/control_002_50r.png'
    directions = []     # array of x,y,angle
    for i, (fname, color) in enumerate(flist):
        # Skip if no track data or track is not in korea
        if len(daum[i]['lati']) > 0 and daum[i]['bbox'].x0 != 1000000:
            if fname[-3:] == 'gpx':
                xl, yl = overlay_gpx(ax, bbox, daum[i], level, color, width=width, method=1)
                dr = build_direction(daum[i], (15-level) * mark)
                draw_markers(ax, dr, color=color, level=level)
            elif fname[-3:] == 'fit':
                xl, yl = overlay_gpx(ax, bbox, daum[i], level, color, width=width, method=1)
                dr = build_direction(daum[i], (15-level) * mark)
                draw_markers(ax, dr, color=color, level=level)
            

            if sf == True:
                draw_icon(ax,fn_start,xl[0],yl[0])
                if pixel_len(xl[0],yl[0],xl[-1],yl[-1]) > 12:
                    draw_icon(ax,fn_end,xl[-1],yl[-1])

            if isway == True and len(daum[i]['ways']) > 0:
                wx,wy,name = calc_wpt(bbox, daum[i]['ways'], level, y_adj=9)
                wy = inverse_y(wy,bbox)
                #draw_icon(ax, fn_waypoint, wx,wy)
                draw_waypoints(ax, fn_waypoint, wx,wy, name, level)

    #ax.show()
    save_path, save_name = os.path.split(flist[0][0])
    save_name = save_name[:-4]

    t_s = time.time()
    #fig.savefig(save_name+'_'+str(level-7)+'.png')
    q = getq(level)
    print(f'Quality factor: {q}')
    fig.savefig(save_name+'_'+str(level-7)+fn_tail+'.jpg',Q=q)
    del(ax)
    del(fig)
    t_s = time.time()-t_s

    t2 = time.time()-t1
    n_file = (bbox.x1-bbox.x0)*(bbox.y1-bbox.y0) 
    if mkind == 2: n_file *= 2
    msg = f'{t2:.3f} sec, {n_file} tiles, {int(n_file/t2)} tiles/sec,    Load: {t_l:.3f}, Save: {t_s:.3f}'
    #print(msg)
    return msg



test_files_pc = [
#[ 'd:/psj/개인자료/자전거/경로/50란도너스/2019/1서울/서울300/S-300K-2.gpx', 'r']
#['D:/psj/개인자료/자전거/경로/50란도너스/2019/1서울/구리300/S-300K-G.gpx', 'b']
['D:/psj/개인자료/자전거/가민/가민500 라이딩/2015-01-09-14-06-25.fit', 'r']       # Garmin Edge500

#['D:/psj/개인자료/자전거/경로/04경상/대구7개령_129.gpx', 'b']

#['D:/psj/개인자료/자전거/경로/50란도너스/2019/1서울/서울600/new/S-600K new2019.gpx', 'g'],
#['D:/psj/개인자료/자전거/경로/50란도너스/2019/2천안/천안600/C-600K-converted.gpx', 'b'],
#['D:/psj/개인자료/자전거/경로/50란도너스/2019/4광주/광주600/G-600K.gpx', 'b'],
#['D:/psj/개인자료/자전거/경로/50란도너스/2019/5부산/부산600/B-600K.gpx', 'b'],
#['D:/psj/개인자료/자전거/경로/50란도너스/2019/6대전/대전600/J-600K_new.gpx', 'g']
#['D:/psj/개인자료/자전거/경로/50란도너스/2019/0그랜드/KOREA1200-old-edited.gpx', 'r']
]
test_files_laptop = [
['e:/psj/자전거/경로/50란도너스/2019/1서울/서울300/S-300K-2.gpx', 'r']
#['D:/psj/개인자료/자전거/경로/테스트/atest_daum_127_38.gpx', 'b']
#['D:/psj/개인자료/자전거/경로/50란도너스/2019/1서울/구리300/S-300K-G.gpx', 'b']
#['e:/psj/자전거/경로/04경상/대구7개령_129.gpx', 'b']

#['e:/psj/자전거/경로/50란도너스/2019/1서울/서울600/new/S-600K new2019.gpx', 'g'],
#['e:/psj/자전거/경로/50란도너스/2019/2천안/천안600/C-600K-converted.gpx', 'b'],
#['e:/psj/자전거/경로/50란도너스/2019/4광주/광주600/G-600K.gpx', 'b'],
#['e:/psj/자전거/경로/50란도너스/2019/5부산/부산600/B-600K.gpx', 'b'],
#['e:/psj/자전거/경로/50란도너스/2019/6대전/대전600/J-600K_new.gpx', 'g']
#['e:/psj/자전거/경로/50란도너스/2019/0그랜드/KOREA1200-old-edited.gpx', 'r']
]

def main():
    if socket.gethostname() == 'PSJ-PC6':
        print('PC detected')
        files = test_files_pc
    else:
        print('Not PC, Laptop assumed')
        files = test_files_laptop

    #do_job( files, level=8)
    #do_job( files, level=9)
    #do_job( files, level=10)
    #do_job( files, mkind=0,level=11)
    print (do_job( files, mkind=0,level=11))
    #do_job( files, mkind=0,level=12)
    #do_job( files, level=13)        # caution! could create very big file
    #do_job( files, level=14)        # caution! could create very big file

def main_test():
    '''
    기준 좌표
    다음 지도 원점 (124.54117113, 32.92846198)
    서울시청 (126.97865367, 37.56682758)
    대변항 태권V등대 (129.23408018, 35.21319250)
    '''

    '''
    #bb = bound(124.541217113,126.97865367,32.92846198,37.56682758)   #원점, 서울시청
    bb = bound(127.,126.,38,37.)   #원점, 서울시청
    print(get_tile_bound(bb,8))
    print(get_tile_bound(bb,9))
    print(get_tile_bound(bb,10))
    print(get_tile_bound(bb,11))
    print(get_tile_bound(bb,12))
    '''

    x = [124.8484884278,124.848621]
    y = [33.47496890081,33.4749750]
    x1,y1 = wgs84_to_daum(x,y)
    print(x[0], x1[0])
    print(y[0], y1[0])


if __name__ == '__main__':
	main()
	#main_test()

