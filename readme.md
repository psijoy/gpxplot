README


**gpxplot 프로그램을 실행하려면 반드시 다음지도 타일을 먼저 가져와야 합니다.**

타일 데이터를 가져오려면 map-get 브랜치를 체크아웃 한 후에 readme 파일을 읽으세요.

- 파이썬 버전 및 환경 : python 3.6.5, anaconda로 설치
- 필요 모듈 : pyqt5, gpxpy, geopy, numpy, matplotlib, PIL(pillow), pyproj
- 주요 모듈은 anaconda 환경에서 conda install 명령으로 설치하거나 conda update 명령으로
버전업 할 수 있습니다. 여기서 설치되지 않으면 pip install 명령으로 설치하시면 됩니다.
혹시 누락이 있을 수 있으므로 모듈이 없다는 오류가 나면 해당 모듈을 추가로 설치하시면 됩니다.

- map_gpx.py의 31~33행: matplotlib의 백엔드를 qt5 환경으로 바꿔줍니다. 단, 기본 qt5 환경이 QT5Agg이기 때문에
속도가 약간 느립니다. QT5cairo가 속도 면에서 유리합니다. 이를 위해서는 cairo 모듈을
추가로 설치해야 합니다(설치하기 귀찮으면 QT5Agg로 변경해서 사용해도 무방합니다)

map_gpx.py 파일에서 get_impath_1909() 함수의 base_path_xxx 값을 본인의 환경에 맞게 수정합니다.
수정할 경로는 다음 지도 타일이 설치된 경로명의 daum_ 까지입니다.
예를 들면,
다음 2D 지도 타일이 설치된 위치가 'F:/Src2/Map/daum_1909dms_2d' 일 경우 base_path_xxx는
'F:/Src2/Map/daum_' 입니다.

- mg_main.py를 처음 실행하게 되면 GUI 화면이 뜨면서 map_gpx_2.cfg라는 설정 파일을 자동 생성합니다.
적절한 gpx 파일을 선택한 후에 각종 옵션을 변경하면서 생성되는 파일을 확인하시면 됩니다.

- 출력되는 jpg 파일은 mg_main.py 가 실행되는 폴더에 생성됩니다.

- 기타: 추가, 개선 예정 사항들은 todo 파일에 기술되어 있습니다.

***주의사항***

- 저장되는 지도 타일을 레벨과 GUI 상에서 선택하는 Map Level은 서로 값이 다릅니다. 이 값들 사이의 매칭은 다음과 같습니다.
GUI level 1 --> Tile level 2
GUI level 2 --> Tile level 3
.   .   .
GUI level 7 --> Tile level 8
