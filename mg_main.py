#!/usr/bin/env python3
# # -*- coding: utf-8 -*-

"""GPX Course Profile Plotter -- main
    Python Version  : 3.5.5 in windows
    Created date    : 2019-11-07
"""
#import ntpath
import os
import sys
#import math
#import copy
import time
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import uic
import gpxpy.parser as parser
from geopy.distance import vincenty
from geopy.distance import great_circle
import configparser
import map_gpx


__author__  = 'Seijung Park'
__license__ = 'GPLv2'
__version__ = '0.2'
__date__    = '2019-11-07'


cfg_filename = 'map_gpx_config.cfg'
cfg_filename2 = 'map_gpx_2.cfg'

def read_cfg_2():
    cfg = []
    s = []
    try:
        f = open(cfg_filename2,'r')
    except FileNotFoundError:
        cfg.append(s)
        cfg.append(0)
        cfg.append(0)
        cfg.append(2)
        cfg.append(1)
        cfg.append(2)
        cfg_dict = dict(file_list=cfg[0], start_stop=cfg[1], waypoint=cfg[2],
                    linewidth=cfg[3], map_level=cfg[4], map_kind=cfg[5],gamma=1.0)
        return cfg, cfg_dict

    try:
        f = open(cfg_filename2,'r')
        n = int(f.readline())
    except (FileNotFoundError,ValueError):
        n = 0

    #print(n,cfg)
    if n > 0:
        for i in range(min(n, 200)):
            s.append(f.readline())
            if s[-1][-1] == '\n':
                s[-1] = s[-1][:-1]
                #print(s[-1])

    cfg.append(s)
    try:
        sf = f.readline()
        if sf[:4] == 'True':
            cfg.append( True )
        else:
            cfg.append( False )
    except ValueError:
        cfg.append( False )

    try:
        wp = f.readline()
        if wp[:4] == 'True':
            cfg.append( True )
        else:
            cfg.append( False )
    except ValueError:
        cfg.append( False )

    try:
        lw = int(f.readline())
        if lw > 10:
            lw = 10
        if lw < 2:
            lw = 2
    except ValueError:
        lw = 4
    cfg.append(lw)

    try:
        ml = int(f.readline())
        if ml > 7:
            ml = 7
        if ml < 1:
            ml = 1
    except ValueError:
        ml = 3
    cfg.append(ml)

    try:
        mk = int(f.readline())
        if mk > 2:
            mk = 2
        if mk < 0:
            mk = 0
    except ValueError:
        mk = 0
    cfg.append(mk)
    cfg_dict = dict(file_list=cfg[0], start_stop=cfg[1], waypoint=cfg[2],
                linewidth=cfg[3], map_level=cfg[4], map_kind=cfg[5],gamma=1.0)
    #cfg_dict = {'file_list':cfg[0], 'start_stop':cfg[1], 'waypoint':cfg[2],
    #            'linewidth':cfg[3], 'map_level':cfg[4], 'map_kind':cfg[5]}
    f.close()
    print('@@@ ',cfg_dict,' @@@')
    return cfg, cfg_dict


def write_cfg_2(cfg):
    print('$$$ ',cfg,' $$$')
    f = open(cfg_filename2,'w')
    n = len(cfg[0])
    f.write(str(n)+'\n')
    for fname in cfg[0]:
        f.write(fname[0]+'\n')

    f.write(str(cfg[1])+'\n')
    f.write(str(cfg[2])+'\n')
    f.write(str(cfg[3])+'\n')
    f.write(str(cfg[4])+'\n')
    f.write(str(cfg[5])+'\n')

    f.close()



def read_cfg():
    s = []
    f = open(cfg_filename,'r')
    try:
        n = int(f.readline())
    except ValueError:
        n = 0

    if n > 0:
        for i in range(min(n, 200)):
            s.append(f.readline())
            if s[-1][-1] == '\n':
                s[-1] = s[-1][:-1]
                print(s[-1])
    f.close()
    #print(s)
    return s


def write_cfg(flist):
    f = open(cfg_filename,'w')
    n = len(flist)
    f.write(str(n)+'\n')
    for fname in flist:
        f.write(fname[0]+'\n')

    f.close()



form_class = uic.loadUiType("map_gpx_ui.ui")[0]

colors_2d = ['b', 'r','c','m','k',
          '#1f77b4',
          '#ff7f0e',
          '#2ca02c',
          '#d62728',
          '#9467bd',
          '#8c564b',
          '#e377c2',
          '#7f7f7f',
          '#bcbd22',
          '#17becf',
          '#1a55FF']
colors_sky = ['b', 'r','c','m','y','k',
          '#1f77b4',
          '#ff7f0e',
          '#2ca02c',
          '#d62728',
          '#9467bd',
          '#8c564b',
          '#e377c2',
          '#7f7f7f',
          '#bcbd22',
          '#17becf',
          '#1a55FF']
colors3 = [
          '#00ffff',   # cyan o0
          '#ff00ff',    # magenta o1
          'b',         # 2
          'r',         # 3
          '#6600ff',   # o4
          '#ee82ee',   # violet o5

        #'#6644ee',      #
        #'#2244dd',
        #'#3377cc',
        #'#e6e6fa',      # lavender
        #'#ffdab9',      # peacjpuff
          #'#ffd700',   # gold
          #'#adff2f',    # greenyellow
          #'#ffff00',    # yellow
          #'#ff4500',    # orangered
          #'#ffa07a',    # lightsalmon
          #'#daa520',    # goldenrod
          #'#ffa500',    # orange
          #'#ffc0cb'    # pink
          ]

class MyWindow(QMainWindow, form_class):
    def __init__(self, flist=[]):
        super().__init__()
        self.setupUi(self)

        self.List_file.clicked.connect(self.do_list_select)

        self.Button_add.clicked.connect(self.do_add_file)
        self.Button_run.clicked.connect(self.do_job)
        self.Button_remove.clicked.connect(self.do_remove_file)
        self.Button_clear.clicked.connect(self.do_clear_list)
        self.Button_exit.clicked.connect(QApplication.instance().quit)
        # quit doesn't call closeEvent()

        self.Combo_kind.currentIndexChanged.connect(self.combo_map_kind)
        self.CB_start.stateChanged.connect(self.cb_start_finish)
        self.CB_way.stateChanged.connect(self.cb_waypoint)
        self.Slider_line.valueChanged.connect(self.sl_line_width)
        self.Slider_map.valueChanged.connect(self.sl_map_level)
        self.Slider_dirmark.valueChanged.connect(self.sl_dir_mark)
        self.Slider_gamma.valueChanged.connect(self.sl_gamma)

        self.Button_clear.setEnabled( False )
        self.Button_remove.setEnabled( False )

        self.lastpath = ''
        self.flist = []
        self.build_flist(flist)
        self.start_finish = False
        self.waypoint = False
        self.line_width = 2
        self.map_level = 1
        self.dir_mark = 60
        self.gamma = 1.0
        self.color_list = colors_2d

        self.Slider_line.minimum = 2
        self.Slider_line.maximum = 10
        self.Slider_line.setValue(self.line_width)
        self.Slider_map.minimum = 1
        self.Slider_map.maximum = 7
        self.Slider_map.setValue(self.map_level)

    def do_list_select(self):
        pass

    def do_add_file(self):
        #flt = 'GPX files(*.gpx,*.fit);;Ride files(*.fit)'
        flt = 'GPX files(*.gpx *.fit)'
        tmp_files = QFileDialog.getOpenFileNames(directory=self.lastpath, filter=flt)[0]
        print(tmp_files)
        if len(tmp_files) > 0:
            self.setpath(tmp_files[0])
            # if tmp_file is not empty and not in flist, append it
            # and update List_file.text
            for fn in tmp_files:
                if fn in self.flist:
                    pass
                else:
                    pos = len(self.flist)
                    self.flist.append([fn,self.color_list[pos%len(self.color_list)]])
                    print(fn)
                    self.List_file.addItem(fn)

            if len(self.flist) > 0:
                self.Button_remove.setEnabled( True )
                self.Button_clear.setEnabled( True )
                self.Status.showMessage('')


    def build_flist(self, fl):
        #print('$$$',fl,'$$$')
        if len(fl) >= 1:
            for i,fn in enumerate(fl):
                #print('^^^',fn,'^^^')
                self.flist.append([fn, self.color_list[i%len(self.color_list)]])
                self.List_file.addItem(fn)

            self.setpath(fl[-1])
            self.Button_remove.setEnabled( True )
            self.Button_clear.setEnabled( True )

    def do_job(self):
        self.Status.showMessage('')
        msg = map_gpx.do_job( self.flist, mkind=self.map_kind, level=self.map_level+7,width=self.line_width,
                       sf=self.start_finish, isway=self.waypoint, gamma=self.gamma, mark=self.dir_mark)
        self.Status.showMessage(f'Finished: {msg}')

    def do_remove_file(self):
        row = self.List_file.currentRow()
        item = self.List_file.takeItem(row)
        if item is not None:
            self.flist.pop(row)
            print(item.text(), row)
        if len(self.flist) < 0:
            self.Button_remove.setEnabled( False )
            self.Button_clear.setEnabled( False )
            

    def do_clear_list(self):
        n = self.List_file.count()
        print('Total deleted: ',n)
        for i in range(n,-1,-1):
            item = self.List_file.takeItem(i)
        self.Button_remove.setEnabled( False )
        self.Button_clear.setEnabled( False )
        self.flist = []
        # todo del list_file contents

    def combo_map_kind(self):
        self.map_kind = self.Combo_kind.currentIndex()

    def cb_start_finish(self):
        self.start_finish = self.CB_start.isChecked()

    def cb_waypoint(self):
        self.waypoint = self.CB_way.isChecked()

    def sl_line_width(self):
        self.line_width = self.Slider_line.value()
        self.Label_lineval.setText(str(self.line_width))

    def sl_map_level(self):
        self.map_level = self.Slider_map.value()
        self.Label_mapval.setText(str(self.map_level))

    def sl_dir_mark(self):
        self.dir_mark = self.Slider_dirmark.value()*10
        self.Label_dirmarkval.setText(str(self.dir_mark))

    def sl_gamma(self):
        self.gamma = self.Slider_gamma.value() / 10.
        self.Label_gammaval.setText(str(self.gamma))

    def setpath(self, path):
        if (os.path.isdir(path)):
            self.lastpath = path
        else:
            self.lastpath = os.path.dirname(path)
        print( 'path after =', self.lastpath)

    def getpath(self):
        return self.lastpath

    def get_flist(self):
        return self.flist

    def build_cfg(self):
        c = []
        c.append(self.flist)
        c.append(self.start_finish)
        c.append(self.waypoint)
        c.append(self.line_width)
        c.append(self.map_level)
        c.append(self.map_kind)
        print('build cfg  @@@',c,'@@@')
        return c

    def set_cfg(self,c,c_dict):
        #print('@@@',c,'@@@')
        #self.flist = c[0]
        self.c_dict = c_dict
        #print(self.c_dict)
        self.build_flist(c[0])

        self.start_finish = c[1]
        self.CB_start.setChecked(c[1])

        self.waypoint = c[2]
        self.CB_way.setChecked(c[2])

        self.line_width = c[3]
        self.Slider_line.setValue(c[3])

        self.map_level = c[4]
        self.Slider_map.setValue(c[4])

        self.map_kind = c[5]
        self.Combo_kind.setCurrentIndex(c[5])

        self.gamma = c_dict['gamma']
        self.Slider_gamma.setValue(self.gamma*10)



def main_simple():
    app = QtWidgets.QApplication(sys.argv)
    filename = QtWidgets.QFileDialog.getOpenFileName(directory='d:/psj/개인자료/자전거/경로/50란도너스/2019',filter='GPX files(*.gpx)')
    if filename[0] and filename[0][-4:] == '.gpx':
        print( filename[0] )
        flist = [[filename[0],'b']]
        map_gpx.do_job( flist, level=12)
    else:
        print("No file seleted to convert.")        #추후 확장자 틀림도 설명할 것



def main():
    #flist = read_cfg()
    cfg, c_dict = read_cfg_2()
    #print( flist, len(flist))

    app = QApplication(sys.argv)
    my_win = MyWindow([])
    my_win.set_cfg(cfg,c_dict)
    my_win.show()
    app.processEvents()

    res = app.exec_()
    #write_cfg(my_win.get_flist())
    cfg = my_win.build_cfg()
    write_cfg_2(cfg)
    sys.exit(res)



if __name__ == '__main__':
	main()
